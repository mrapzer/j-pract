//
// Created by Gabe Blandin on 2/17/2023.
// This is the secondary main entry point for this hello_world project
// This is based on the docs at https://cplusplus.com/doc/tutorial/
//
#include <iostream>
#include <sstream>
#include <array>
#include <utility>

using namespace std;

#define DEFINITION "this is a definition"

// Variable names in prototypes is optional but recommended
void test_function_by_value(string a, int b);
bool test_function_by_reference(string& a, int& b);
inline string concatenate(const string& a, const string& b);
int function_with_default(int a=1, int b=2, int c=3);
// Overloaded
void test_function_by_value(int a, int b, int c);

// Templates
template <class T>
T template_function(T a, T b);

// Namespaces
namespace test_namespace {
    int abc, xyz;
    string get_name() {
        return "test_namespace";
    }
}

// Would probably make more sense to be in test_namespace but this is for demonstration
int add_namespace_values();

void print_array(int arg[], int size);
void print_2D_3_deep_array(int arg[][3], int size); // Note the dimensions past the first MUST have specified size

void func_with_func_ptr_arg(int x, int (*func)(int));
int matching_ptr_func(int x);

class ExampleClass {
    // Below members are private automatically
    int a, b;
    char c, d;
    string name;
public: // Accessible from anywhere
    static int e;
    ExampleClass () {a=1;b=2;c='c';d='d';name="default";}; // Default constructor
    ExampleClass (int,char,string); // Note the constructor has no return type
    ExampleClass (int,string);
    void set_a (int);
    // Inline definition
    string get_values () {return name + to_string(a) + to_string(b) + c + d;};
    const string& get_values () const {
        static const string values = name + to_string(a) + to_string(b) + c + d;
        return values;
    };
    friend ExampleClass duplicate(const ExampleClass&);
protected: // Accessible from this class or derived classes
    void set_c (char input) {c = input;};
private: // Accessible only within class
};

int ExampleClass::e=5;

ExampleClass::ExampleClass(int givenInt, char givenChar, string givenString) {
    a = givenInt;
    b = 0;
    c = 'c';
    d = givenChar;
    name = std::move(givenString); // Move was suggested optimization
}

// Member initialization constructor; these can also use braces instead
ExampleClass::ExampleClass(int a, string name) : a(a), name(std::move(name)) {b=2;c='c';d='d';}

// Outside definition
void ExampleClass::set_a(int input) {
    a = input;
}

// Friend has access to private members
ExampleClass duplicate(const ExampleClass& given) {
    ExampleClass result;
    result.a = given.a;
    result.b = given.b;
    result.c = given.c;
    result.d = given.d;
    result.name = given.name;
    return result;
}

template <class T>
class TemplateClass {
    T value;
public:
    TemplateClass(T given) {value = given;};
};

// Specialization for char
template <>
class TemplateClass<char> {
    // Does not inherit anything from the other template
    char value;
public:
    TemplateClass(char given) {value=given;};
};

int main() {
    // Data types
    char letter = 'a';
    int num = 1;
    unsigned int unsign_num = 2;
    long long_num = 3;
    long long big_num = 4;
    float decimal = 5.0;
    double precise_decimal = 6.0; // This is default for floating point
    long double big_decimal = 7.0;
    bool boolean = true;
    //void

    // Initializations
    int test = 8; // C-like initialization
    int test_2 (9); // Constructor initialization (introduced by C++)
    int test_3 {10}; // Uniform initialization (introduced by C++11)

    // Type deduction
    int foo = 11;
    auto bar = foo; // Compiler knows this should be an int because foo is
    auto baz = 12; // Same here more or less
    decltype(foo) qux = 13; // Another way the compiler can infer typing

    // Strings
    string hello_world = "Hello World!";
    cout << hello_world << endl;

    // Literals
    int dec = 14;
    int oct = 017; // Decimal 15
    int hex = 0x10; // Decimal 16
    int unsign_2 = 17u;
    int scientific = 1.8e1; // 18
    int float_num = 19.0f;
    string big_string = "this " "is " "a "
                                    "really "
                                    "big "
                                    "string";
    cout << big_string << endl;
    string raw = R"sequence(string with a \backslash)sequence";
    cout << raw << endl;
    int* p = nullptr; // null pointer value; could also be 0

    // Constants
    const int constant = 20;
    // Preprocessor stuff (see the "define" above)
    cout << DEFINITION << endl;

    // Assignment and math
    // This is an interesting example from the tutorial, maybe not useful, but interesting
    int x, y, z;
    y = 2 + (x = 5); // y becomes 7
    x = y = z = 21; // Right to left assignment
    // All the normal math ops, + - * / and %
    // All the normal compound +=, -=, etc. and bitwise stuff >>=, &=, etc.
    // We also have normal increment and decrement ++ -- with pre and post incrementing
    // All the normal comparison stuff == != > < >= <= and logical stuff ! && ||
    // Ternaries exist as expected

    // Comma operator
    // "used to separate two or more expressions that are included where only one expression is expected"
    int a, b;
    a = (b = 3, b + 2); // a becomes 5 and b is 3

    // bitwise stuff as normal & | ^ ~ << >>

    // Type casting
    float cast_float = 22.0;
    int c = (int) cast_float;
    int d = int (cast_float);

    size_t type_size = sizeof (int);

    // Operator Precedence, all are grouping left to r except specified
    // Scope ::
    // Postfix ++ -- () [] . ->
    // Prefix (R to L) ++ -- ~ ! + - & * new delete sizeof (type)
    // Pointer to member .* ->*
    // Math * / %
    // Math + -
    // Bitwise shift << >>
    // Relational < > <= >=
    // Equality == !=
    // And &
    // Exclusive or ^
    // Inclusive or |
    // Conjunction &&
    // Disjunction ||
    // Assignment (R to L) = *= /= %= += -= >>= <<= &= ^= |= ?:
    // Sequencing ,
    // "When an expression has two operators with the same precedence level, grouping determines which one is evaluated first: either left-to-right or right-to-left."

    // Basic IO
    cout << "Basic String " << a << " " << 21 << endl;
    int int_input;
    string str_input;
    // This isn't really proper form since cin will fail silently if expected type != given type
    // Also note, getting a string is whitespace delimited so only a single word will be retrieved
    cout << "Enter a number, then enter a string" << endl;
    cin >> int_input >> str_input;
    cout << "Int given is " << int_input << " string given is " << str_input << endl;
    // To get a full line
    cout << "Enter a full line of text:" << endl;
    cin.ignore(); // Not proper format to use both >> and getline but this is just a test program so using this fix to get around trailing new line
    getline(cin, str_input);
    cout << "Line given is " << str_input << endl;
    // stringstream
    string stream_test ("2048");
    int stream_int;
    stringstream(stream_test) >> stream_int;
    cout << "Stream test: " << stream_int << endl;
    string number_test_str;
    int number_test;
    cout << "Enter number:";
    getline(cin, number_test_str);
    stringstream(number_test_str) >> number_test;
    cout << "Number was: " << number_test << endl;

    // Flow control
    // Normal if else if etc conditions apply
    // Normal while, do while, for, and for each (or range) loops
    // for each works on any type that supports the begin and end functions
    // break and continue also exist and work as expected
    // goto exists (and as expected has few uses in modern day)
    // Note goto ignores nesting levels so should be used with care
    bool has_jumped = false;
test_label:
    if (!has_jumped) {
        has_jumped = true;
        cout << "Jumping" << endl;
        goto test_label;
    }
    cout << "Jumped" << endl;
    // switch also exists and works as expected and has fall through and a default
    // switch conditions MUST be constant expressions

    // Functions
    // Keep in mind by reference and by value
    test_function_by_value("abc", 23);
    string str_reference = "abc";
    int int_reference = 24;
    cout << "The reference values are " << str_reference << " and " << int_reference << endl;
    bool did_update = test_function_by_reference(str_reference, int_reference);
    if (did_update)
        cout << "The updated reference values are " << str_reference << " and " << int_reference << endl;
    else // Shouldn't happen but included for completeness
        cout << "Reference values were not updated" << endl;
    // Inline functions
    cout << "Combined string is: " << concatenate("abc", "def") << endl;
    // Defaults
    int fun_result = function_with_default();
    cout << "Default result " << fun_result << endl;
    fun_result = function_with_default(4);
    cout << "Two defaults result " << fun_result << endl;
    fun_result = function_with_default(4, 8, 6);
    cout << "No defaults result " << fun_result << endl;

    // Overloads and templates
    test_function_by_value(1, 2, 3);
    // Functions cannot be overloaded on return type alone
    // Templates
    string template_result_str = template_function<string>("abc", "def");
    cout << "Template result: " << template_result_str << endl;
    int template_result_int = template_function(2, 2);
    cout << "Template result: " << template_result_int << endl;
    // Templates can contain multiple types (class T, class U, etc.)
    // They can also take in literals <class T, int N> and usage <int, 4> will make T type int and uses of N 4
    // The second argument MUST be a constant expression

    // Name visibility
    // Scope
    // global and local exist and name collisions are not allowed to occur within the same scope
    // local variables can however redefine global variables
    // Namespaces
    // Declaration must be in global scope
    cout << "Test Namespace Name: " << test_namespace::get_name() << endl;
    test_namespace::abc = 26;
    test_namespace::xyz = 27;
    cout << "Namespace values: " << test_namespace::abc << " " << test_namespace::xyz << endl;
    // Note namespaces can be split
    // As in test_namespace values could exist in two files and all values in that namespace would be within that namespace
    // using keyword
    // The using keyword brings a namespaces values into the current scope such as with cout which is actually std::cout
    int namespace_sum = add_namespace_values();
    cout << "Sum of namespace values: " << namespace_sum << endl;
    // This is an interesting example of splitting namespace use into blocks within the same function
    //    int main () {
    //        {
    //            using namespace first;
    //            cout << x << '\n';
    //        }
    //        {
    //            using namespace second;
    //            cout << x << '\n';
    //        }
    //        return 0;
    //    }
    // Namespaces can be aliased using
    namespace new_name = test_namespace;
    cout << "Aliased namespace name function: " << new_name::get_name() << endl;
    // NOTE global variables (or more accurately variables with static storage) that are not explicitly initialized are auto initialized to 0
    // automatic storage variables that are uninitialized have undetermined value

    // Arrays
    // Array size MUST be a CONSTANT expression
    int int_array [5]; // Undetermined values but does technically contain values
    // Size must be >= number of elements to init with, note the remaining are set to 0
    int int_array_2 [4] = { 26, 27 }; // [26, 27, 0, 0]
    int int_array_3 [2] = { }; // Inits elements to 0, i.e. [0, 0]
    int int_array_4 [] = { 28 }; // Automatically infers size to be 1
    int int_array_5 [] { 29, 30, 31, 32}; // Arrays can also be initialized without the = (this is called universal initialization)
    // Note from docs: Static arrays, and those declared directly in a namespace (outside any function), are always initialized.
    //                 If no explicit initializer is specified, all the elements are default-initialized (with zeroes, for fundamental types)
    // Standard accessing and setting for arrays is present
    // Note that there are exists no kind of index out of bounds exception so caution should be used when getting elements from an array
    // Multidimensional
    // No limit to number of dimensions but memory needed increases exponentially
    int multi_array [3][3] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
    // Fun tidbit from the tutorial
    // char century [100][365][24][60][60]; declares an array with elements for each second in a century. This is 3 billion chars and would consume over 3 GB of memory
    // Arrays as parameters
    print_array(int_array_5, 4);
    // Note the dimensions past the first MUST have specified size in the function declaration
    print_2D_3_deep_array(multi_array, 3);
    // Library arrays
    // These include some helpful methods and such
    array<int, 3> lib_array {33, 34, 35};
    cout << "There are " << lib_array.size() << " elements in the lib array. They are: ";
    for (int item : lib_array) {
        cout << item << " ";
    }
    cout << endl;

    // Character sequences
    // All strings are (clearly) arrays of chars and so can be represented as such
    char test_word[] = { 'H', 'e', 'l', 'l', 'o', '\0' };
    // The following expressions would be invalid since the array is initialized and declared simultaneously
    // test_word = "bye";
    // test_word[] = "bye";
    // test_word = {'B', 'y', 'e'};
    // Values in the array can still be modified though
    // This declaration approach would be considered a C-string
    char another_word[] = "Yo";
    // Important difference between using char x[] and string is the array declaration is a constant size and strings are dynamic
    // Arrays can be converted implicitly and strings can be converted using c_str() or data()

    // Pointers
    int test_int = 36;
    int* test_int_ptr = &test_int;
    int test_int_val = *test_int_ptr;
    *test_int_ptr = 37;
    // First one will be 37 since the pointers value (which points to test_int) changed while the second is still 36 since that was the value at assignment time
    cout << "First int is now: " << test_int << " and the second is now: " << test_int_val << endl;
    // Arrays can be converted to pointers since they are effectively the same
    int test_array[4];
    int* test_array_ptr = test_array;
    // Main difference in above is that test_array_ptr can be reassigned to a different address, but test_array cannot be reassigned
    // Example from the docs
    int ex_array[5]; // indeterminate values contained
    int* array_p;
    array_p = ex_array;  *array_p = 10; // [10, x, x, x, x]
    array_p++;  *array_p = 20; // [10, 20, x, x, x]
    array_p = &ex_array[2];  *array_p = 30; // [10, 20, 30, x, x]
    array_p = ex_array + 3;  *array_p = 40; // [10, 20, 30, 40, x]
    array_p = ex_array;  *(array_p+4) = 50; // [10, 20, 30, 40, 50]
    // for (int n=0; n<5; n++) // This is what is in the example, but a range based loop is recommended
    //    cout << ex_array[n] << ", ";
    for (int n : ex_array)
        cout << n << ", ";
    cout << endl;
    // Couple notes from the docs
    // a[5] = 0;       // a [offset of 5] = 0
    // *(a+5) = 0;     // pointed to by (a+5) = 0
    // These two expressions are equivalent and valid, not only if a is a pointer, but also if a is an array
    // Only addition and subtraction are valid on pointers
    // When doing pointer arithmetic, the pointer will move the size of the values data type in memory addresses
    // e.g. ++charPtr moves one byte, ++shortPtr two bytes, ++longPtr three bytes, and so on
    // Note *p++ is equal to *(p++) since postfix operators have higher precedence than prefix
    // Examples from docs:
    // *p++   // same as *(p++): increment pointer, and dereference unincremented address
    // *++p   // same as *(++p): increment pointer, and dereference incremented address
    // ++*p   // same as ++(*p): dereference pointer, and increment the value it points to
    // (*p)++ // dereference pointer, and post-increment the value it points to
    int test_read_ptr = 38;
    const int* const_ptr = &test_read_ptr; // This value this pointer points to cannot be modified
    // Note that int* is implicitly converted to const int* but const int* cannot be converted to int*
    // Use case for this is marking function arguments that the function will only read (such as for printing)
    // Pointers can also be const i.e.
    int test_const_ptr;
          int *       p1 = &test_const_ptr; // non-const pointer to non-const int (pointer and value can be changed)
    const int *       p2 = &test_const_ptr; // non-const pointer to const int     (pointer can be changed, value cannot)
          int * const p3 = &test_const_ptr; // const pointer to non-const int     (pointer cannot be changed, value can)
    const int * const p4 = &test_const_ptr; // const pointer to const int         (pointer and value cannot be changed)
    // Important note, const can be before or after type (but before *) and have the same effect
    // String literals are essentially const char* values so the values inside the string cannot be modified
    // Multi-dimensional pointers are allowed e.g. int ** c;
    // void pointers are also allowed, essentially an "any" typing, but they cannot be directly dereferenced
    // void pointers are useful in generic functions
    // Pointers can technically point to any address, even out of bounds of an array or while being uninitialized
    // Dereferencing an out-of-bounds pointer could cause an error however or just undefined behavior
    // Function pointers are also allowed, namely for us when passing a function as a function argument
    func_with_func_ptr_arg(39, matching_ptr_func);

    // Dynamic memory
    // Uses new and delete (with optional [] specifying number to allocate)
    int* alloc_ptr;
    alloc_ptr = new int; // Allocates one int
    int* multi_alloc_ptr;
    multi_alloc_ptr = new int[3]; // Allocates 3 ints
    // Any variable can be used to specify this size
    // Note that technically speaking, new may fail to allocate if memory is already exhausted or requested is too large
    // There are 2 ways to check though, through exceptions (bad_alloc, default) and nothrow
    // When using nothrow, it returns a nullptr
    // e.g.
    // foo = new int [5];  // if allocation fails, an exception is thrown
    // foo = new (nothrow) int [5]; // if allocation fails, foo is nullptr
    delete alloc_ptr;
    delete[] multi_alloc_ptr;
    // nullptr can be passed to delete but produces no effect
    // C allocations can also be used if desired (like malloc, free, etc) but should not be mixed with new and delete

    // Data structures
    struct data_type { // When specifying objects, this name is optional; here we use it to declare a third object though
        int number;
        string label;
    } type1, type2; // Can be declared here; can also declare an array here
    data_type type3; // or like this
    type1.number = 40;
    type1.label = "yep";
    auto *type4 = new data_type;
    type4->number = 41; // dereferencing to attribute
    // And of course structs can be nested

    // Other data types
    // typedef and using create aliases
    typedef int int_alias; // typedef inherited from C
    using string_alias = string; // This way is more generic due to limitations in templates typedef has
    // Unions allow one address to be accessed as one of any data type
    union test_union {
        int i;
        char c;
    };
    // Neat example that occupies the same memory (assuming 4 byte int)
    union mix_t {
        int l; // 4 byte
        struct {
            short hi; // 2 byte
            short lo; // 2 byte
        } s;
        char c[4]; // 4 1 byte chars
    };
    // When used in a struct, no name is required thus creating an anonymous union
    struct anonymous_union {
        union {
            int a;
            float b;
        };
    };
    // We can access values with ob.a or ob.b whereas without we'd need ob.union_name.a or ob.union_name.b
    // enums do not include another type (but are implicitly ints starting at 0 unless specified)
    enum colors_t {black, blue, green, cyan, red, purple, yellow, white};
    colors_t ex = blue; // This works since the enum is itself a type
    // A number can be specified in the enum and unless explicit, the rest are the previous number + 1
    // Enum classes are also possible and as such would not be ints implicitly
    enum class Colors {black, blue, green, cyan, red, purple, yellow, white};
    Colors ex2 = Colors::black;
    // Size can be specified by specifying the underlying type
    enum class EyeColor : char {blue, green, brown};

    // Classes
    // Similar to structs but allow inclusion of functions and access specifiers (public, private, protected)
    // See example class
    // Only difference between internal and external function definitions is compiler optimization
    // The constructor has no return type so the following is instantiation
    ExampleClass obj (42, 'd', "example");
    ExampleClass def; // This calls the default constructor; including parentheses would NOT call the default constructor
    ExampleClass def2 {}; // Also calls the default constructor
    class SingleVal {
        friend class ExampleClass; // Declaring friend class
    public:
        int value;
        SingleVal(int a) {value=a;};
        ~SingleVal() {}; // Destructor
        SingleVal(const SingleVal& x) : value(x.value) {}; // Copy constructor
        SingleVal operator + (const SingleVal& given) {
            SingleVal result {value + given.value};
            return result;
        };
        SingleVal& operator = (const SingleVal& given) { // Copy assignment
            value = given.value;
            return *this;
        }
        SingleVal (SingleVal&&); // Move constructor; Ex if this used pointers: ptr(x.ptr) {x.ptr=nullptr;}
        SingleVal& operator = (SingleVal&&); // Move assignment
    };
    SingleVal s1 (43); // Functional initialization
    SingleVal s2 = 44; // Assignment initialization (calls single param constructor)
    SingleVal s3 {45}; // Can be called with multiple values
    SingleVal s4 = {46}; // Equal sign is optional
    // Note that when using member initialization, object members that are not initialized after the colon (that aren't fundamental types)
    // get default-construction. When default construction is not possible, members shall be initialized in the member initialization list
    SingleVal *s5 = &s4;
    cout << s5->value << endl;
    // Note the keyword struct can also be used for classes with member functions, but they have public access by default
    // Also note that union types can also hold member functions and have default access of public
    // Operators can be overloaded to allow types to have additional behavior
    // Overloadable operators
    // + - * / % ^ & | ~ ! = < > += -= *= /= %= ^= &= |= << >> >>= <<= == != <= >= <=>(C++20) && || ++ -- , ->* -> () []
    // SingleValue above overloads the + operator
    SingleVal s6 = s1 + s2; // Can also be called with s1.operator+ (s2)
    // Different operators require different arguments
    // Some are able to be declared as non-member functions as well
    // "this" is a pointer to the current object (see above example, which would be similar to implicitly generated operator)
    // Static/class variables are constant across instances
    // Static members cannot be initialized inside the class
    // Static members can be referred to on an instance (though not recommended) or the class itself
    cout << ExampleClass::e << def2.e << endl;
    // Classes can also have static functions, but they cannot access non-static members or the "this" keyword
    // Declaring an object with const it becomes read only from the outside and only const member functions can be called
    // int get() const {return x;}        // const member function
    // const int& get() {return x;}       // member function returning a const&
    // const int& get() const {return x;} // const member function returning a const&
    // Const member functions can't modify non-static data or call non-const functions
    // Non-const functions can call const functions though
    // Functions can be overloaded with const and non-const versions
    const ExampleClass const_def;
    const string& const_def_vals = const_def.get_values();
    cout << const_def_vals << endl;
    // Class templates are also possible and similar to function templates
    TemplateClass<int> template_class_instance (47);
    // Outside function definitions for template classes must be preceded with template <class T>
    // Template specialization for when a specific type is given

    // Special members
    // Default, destruct, copy constructor, copy assignment, move constructor, move assignment
    // Destructors can be used to delete pointers or release other resources
    // Copy and move constructors are implicit if not explicitly provided and performs a shallow copy or move
    // When a class includes pointers or other such types, a custom copy/move constructor is strongly recommended
    // Otherwise, if those resources are released in the destructor, the first one to be destroyed could potentially destroy
    // the value the second one is pointing to thus losing data
    // This example was given for copy assignments
    // MyClass foo;
    // MyClass bar (foo);       // object initialization: copy constructor called
    // MyClass baz = foo;       // object initialization: copy constructor called
    // foo = bar;               // object already initialized: copy assignment called
    // Copy assignment is an overload of the = operator and takes by reference or value
    // One item of note for copy assignment is the potential for memory leakage when changing a pointer reference
    // Move constructor and assignment removes item from source. This only occurs on unnamed objects
    // Used when object is initialized or assigned from unnamed temporary
    // Examples
    // MyClass fn();            // function returning a MyClass object
    // MyClass foo;             // default constructor
    // MyClass bar = foo;       // copy constructor
    // MyClass baz = fn();      // move constructor
    // foo = bar;               // copy assignment
    // baz = MyClass();         // move assignment
    // Type&& is an rvalue reference and matches temporaries of the type
    // rvalue references should be avoided as they are rarely useful outside of the move operations
    // Note when special members are implicitly defined:
    // Default constructor: if no other constructors
    // Destructor: if no destructor
    // Copy constructor/assignment: if no move constructor and no move assignment
    // Move constructor/assignment: if no destructor, no copy constructor and no copy nor move assignment
    // Defaults can be explicitly used (or not used) using the default or delete keyword
    // function_declaration = default;
    // function_declaration = delete;
    // Keep in mind this note:
    // "In general, and for future compatibility, classes that explicitly define one copy/move constructor or one copy/move assignment
    // but not both, are encouraged to specify either delete or default on the other special member functions they don't explicitly define."

    // Friendship and inheritance
    // private and protected members can be accessed in non-member functions declared friend (external function)
    ExampleClass def3;
    def3 = duplicate(def2);
    // Classes can also be friends
    // Note in SingleVal that ExampleClass is its friend, but not vice versa
    // I didn't include any good examples of usage here since SingleVal is declared inside main, but ExampleClass would have
    // access to SingleVal's private and protected members
    // Also note that a friend of a friend is not inherently a friend unless explicitly declared
    // Defining inheritance is done on derived class:
    // class ClassName: public BaseClass
    // public can be replaced with private or protected and sets the access level for inherited members, private is assumed if none is specified for classes or public for struct
    // "The members with a more accessible level are inherited with this level instead, while the members with an equal or more restrictive access level keep their restrictive level in the derived class."
    // Most cases should use public
    // Constructor and destructor, assignment operator, friends, and private members are not inherited (but constructor and destructor of base are automatically called)
    // Calling non-default constructor is possible with: derived_constructor_name (parameters) : base_constructor_name (parameters) {...}
    // Multiple inheritance is also possible

    // Polymorphism
    // Pointers to derived classes are type compatible with pointers to base class
    // Virtual members can be defined on base classes and be redefined on derived classes
    // A redeclared member that doesn't use virtual will just use the variable's type implementation (so pointer to a base class won't use the redefined version unless it is virtual)
    // Abstract classes can only be used as base classes; They must contain at least one pure virtual function
    // Pure virtual members use =0 syntax instead of a definition: virtual int area () { return 0; } vs virtual int area () =0;
    // Abstract classes cannot be used to instantiate objects, only as pointers

    return 0;
}

void test_function_by_value(string a, int b) {
    // The parameter wants to be "const string& a" to prevent copying
    // (since string could potentially be large) and mark the value as non-changing
    // We won't do that here just because this function is for by value but that would be ideal in normal use
    cout << "The string A is: " << a << endl;
    cout << "The int B is: " << b << endl;
}

bool test_function_by_reference(string& a, int& b) {
    if (b == 24) {
        a = "def";
        b = 25;
        return true;
    } else { // Shouldn't happen but included for completeness
        return false;
    }
}

// Example taken from the docs
// Referring to the inline keyword
// "This informs the compiler that when concatenate is called, the program prefers the function to be expanded inline,
// instead of performing a regular call. inline is only specified in the function declaration, not when it is called."
// "Note that most compilers already optimize code to generate inline functions when they see an opportunity to improve
// efficiency, even if not explicitly marked with the inline specifier. Therefore, this specifier merely indicates
// the compiler that inline is preferred for this function, although the compiler is free to not inline it, and
// optimize otherwise."
inline string concatenate(const string& a, const string& b) {
    return a + b;
}

int function_with_default(int a, int b, int c) {
    return (a+b) / c;
}

void test_function_by_value(int a, int b, int c) {
    cout << "The int A is: " << a << endl;
    cout << "The int B is: " << b << endl;
    cout << "The int C is: " << c << endl;
}

template <class T>
T template_function(T a, T b) {
    return a + b;
}

int add_namespace_values() {
    // Alternatively, we can specify specific values such as
    // using test_namespace::xyz or using test_namespace::get_name
    // These would only pull xyz or get_name into the scope to be used unqualified
    using namespace test_namespace;
    cout << "Adding namespace values for namespace: " << get_name() << endl;
    return abc + xyz;
}

void print_array(int arg[], int size) {
    cout << "Printing array: [ ";
    for (int i = 0; i < size; i++) {
        cout << arg[i] << " ";
    }
    cout << "]" << endl;
}

void print_2D_3_deep_array(int arg[][3], int size) {
    cout << "Printing 2D array: [\n";
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < 3; j++) {
            cout << arg[i][j] << " ";
        }
        cout << "\n";
    }
    cout << "]" << endl;
}

void func_with_func_ptr_arg(int x, int (*func)(int)) {
    cout << "Got: " << x << " and after passing: " << (*func)(x) << endl;
}

int matching_ptr_func(int x) {
    return 2 * x;
}
